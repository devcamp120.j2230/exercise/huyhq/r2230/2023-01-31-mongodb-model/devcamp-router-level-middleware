const express = require("express");

//khai báo thư viện mongoDb
const mongoose = require("mongoose");

const reviewModel = require("./app/model/reviewModel");
const courseModel = require("./app/model/courseModel");

const { courseRouter } = require("./app/routes/courseRoute");

const app = express();

const post = 8000;

//connect tới table mongoDb
const nameDb = "CRUD_Course";
mongoose.connect("mongodb://127.0.0.1:27017/" + nameDb, function (error) {
    if (error) throw error;
    console.log('Successfully connected to DB: '+nameDb);
})

//khai báo middleware
app.use(express.json());
//get router
app.use("/", courseRouter);

// app.get("/", (req, res) => {
//     let body1 = req.body;
//     res.status(200).json({
//         body1,
//     })
// })

app.listen(post, () => {
    console.log("Run App on port: " + post);
})