//khai báo thư viện mongoose
const mongoose = require("mongoose");
//khai báo schema từ thư viện
const Schema = mongoose.Schema;
//tạo đối tượng Schema với các thuộc tính yêu cầu
const courseSchema = new Schema({
    _id : {
        type : mongoose.Types.ObjectId,
    },

    title: {
        type : String,
        required : true,
        unique : true
    },

    decription : {
        type : String,
        required : false
    },
    review : [{
        type : mongoose.Types.ObjectId,
        ref : "review",
    }]
});
//export Schema ra Model có tên "review"
module.exports = mongoose.model("course", courseSchema);



