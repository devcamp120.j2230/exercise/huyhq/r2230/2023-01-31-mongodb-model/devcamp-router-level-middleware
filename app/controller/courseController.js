const { default: mongoose } = require("mongoose");
const courseModel = require("../model/courseModel");

const getAllCoursesController = (req, res) => {
    //lấy toàn bộ dữ liệu trong mongoDb
    courseModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: `server error: ${error.message}`           //trả về lỗi
            })
        } else {
            return res.status(200).json({
                message: `Load all data success!`,
                course: data
            })
        }
    });
};

const getACourseController = (req, res) => {
    var id = req.params.id;
    //kiểm tra id có đúng định dạng
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: 'Id is invalid',
        })
    } else {
        //lấy dữ liệu ứng với id
        courseModel.findById(id, (error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `server error: ${error.message}`           //trả về lỗi
                })
            } else {
                if (data) {
                    return res.status(200).json({
                        message: `Load data id success!`,
                        course: data
                    })
                } else {
                    return res.status(200).json({
                        message: `Id ${id} not found!`
                    })
                }

            }
        })
    }
};

const postACourseController = (req, res) => {
    //lấy body
    var body = req.body;

    //kiểm tra body có title không
    if (!body.title) {
        return res.status(400).json({
            message: "title is required!"
        })
    }

    //tạo đối tượng trong model
    let newCourse = new courseModel({
        _id: mongoose.Types.ObjectId(),                             //tự tạo một id bất kỳ
        title: body.title,
        decription: body.decription
    })

    //thêm dữ liệu vào mongoDb
    courseModel.create(newCourse, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `server error: ${error.message}`           //trả về lỗi
            })
        } else {
            return res.status(201).json({
                message: `Post success!`,
                course: data
            })
        }
    })
};

const putACourseController = (req, res) => {
    var id = req.params.id;         //lấy id
    var body = req.body;            //lấy body sửa
    //kiểm tra id có đúng định dạng
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: 'Id is invalid',
        })
    } else {
        //kiểm tra body có title không
        if (!body.title) {
            return res.status(400).json({
                message: "title is required!"
            })
        }

        //tạo đối tượng trong model
        let editCourse = new courseModel({
            title: body.title,
            decription: body.decription
        })

        //thêm đối tượng vào id tương ứng
        courseModel.findByIdAndUpdate(id,editCourse, (error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `server error: ${error.message}`           //trả về lỗi
                })
            } else {
                return res.status(201).json({
                    message: `Edit success!`,
                    course: data
                })
            }
        })
    }

};

const deleteACourseController = (req, res) => {
    var id = req.params.id;         //lấy id
    var body = req.body;            //lấy body sửa
    //kiểm tra id có đúng định dạng
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: 'Id is invalid',
        })
    } else {
        //kiểm tra body có title không
        if (!body.title) {
            return res.status(400).json({
                message: "title is required!"
            })
        }

        //xóa đối tượng với id tương ứng
        courseModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `server error: ${error.message}`           //trả về lỗi
                })
            } else {
                return res.status(204).json({
                    message: `Delete success!`,
                    course: data
                })
            }
        })
    }
};

module.exports = {
    getAllCoursesController,
    getACourseController,
    postACourseController,
    putACourseController,
    deleteACourseController
};