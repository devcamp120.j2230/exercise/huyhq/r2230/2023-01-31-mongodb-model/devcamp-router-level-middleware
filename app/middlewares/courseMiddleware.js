const getAllCourses = (req,res,next)=>{
    console.log("Get All Courses!");
    next();
};

const getACourse = (req,res,next)=>{
    console.log("Get a Course!");
    next();
}

const postACourse = (req,res,next)=>{
    console.log("Create new Course!");
    next();
}

const putACourse = (req,res,next)=>{
    console.log("Update a Course!");
    next();
}

const deleteACourse = (req,res,next)=>{
    console.log("Delete a Course!");
    next();
}

module.exports = {
    getAllCourses,
    getACourse,
    postACourse,
    putACourse,
    deleteACourse,
}